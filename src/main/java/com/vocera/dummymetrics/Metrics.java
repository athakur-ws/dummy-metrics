package com.vocera.dummymetrics;

import java.util.Date;

public class Metrics {

    private Integer podsCount;
    private Integer metricX;
    private Date date;

    public Metrics(Integer podsCount, Integer metricX, Date date) {
        this.podsCount = podsCount;
        this.metricX = metricX;
        this.date = date;
    }

    public Integer getPodsCount() {
        return podsCount;
    }

    public void setPodsCount(Integer podsCount) {
        this.podsCount = podsCount;
    }

    public Integer getMetricX() {
        return metricX;
    }

    public void setMetricX(Integer metricX) {
        this.metricX = metricX;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

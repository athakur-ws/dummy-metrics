package com.vocera.dummymetrics;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1ObjectMeta;
import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.openapi.models.V1PodList;
import io.kubernetes.client.util.ClientBuilder;
import io.kubernetes.client.util.Config;
import io.kubernetes.client.util.KubeConfig;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
class MetricsRestController {

    private static final String KUBE_CONFIG_PATH = "~/.kube/config";
    private static final int[] FAKE_LOAD = {50, 70, 120, 130, 120, 180, 200, 180, 150, 120, 100, 80};
    private static final String PREFACE_CONTENT = "# HELP metric_x Get dummy metrics\n" +
            "# TYPE metric_x gauge\n\n";
    private static final String METRIC_FORMAT = "metric_x %d";

    private List<V1Pod> getPods() throws IOException, ApiException {

        ApiClient client = ClientBuilder.cluster().build();
        Configuration.setDefaultApiClient(client);

        CoreV1Api api = new CoreV1Api();
        V1PodList list = api.listPodForAllNamespaces(null, null, null, null, null, null, null, null, null);
        return list.getItems();
    }

    @RequestMapping("/pods")
    public String getPodsInfo() {
        List<V1Pod> list = null;
        String res = "";
        try {
            list = getPods();
            res = list.stream().map(V1Pod::getMetadata).map(V1ObjectMeta::toString).collect(Collectors.joining());
        } catch (IOException | ApiException e) {
            res = e.toString();
        }
        return res;
    }

    @RequestMapping("/metrics-json")
    public Metrics getMetrics() {

        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        calendar.setTime(date);
        int index = calendar.get(Calendar.SECOND) / 5; // 0 to 59 sec --> 0 to 11
        int podsCount = -1;
        try {
            List<V1Pod> pods = getPods();
            podsCount = pods.size();
        } catch (IOException | ApiException e) {
            e.printStackTrace();
        }
        Integer metricX =  FAKE_LOAD[index] / podsCount;
        return new Metrics(podsCount, metricX, date);
    }

    @RequestMapping(value = "/metrics", method = RequestMethod.GET, produces = "text/plain")
    @ResponseBody
    public String getPrometheusMetrics() {
       Metrics metrics = getMetrics();
       return toPrometheusFormat(metrics);
    }

    public static String toPrometheusFormat(Metrics metrics) {
        String sb = PREFACE_CONTENT + String.format(METRIC_FORMAT, metrics.getMetricX()) +
                "\n\n";
        return sb;
    }
}

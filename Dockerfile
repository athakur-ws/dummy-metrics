FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/dummy-metrics-0.0.1-SNAPSHOT.jar dummy-metrics.jar
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /dummy-metrics.jar" ]